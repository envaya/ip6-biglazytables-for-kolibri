/**
 * Describes the object used for initializing and testing a table component.
 * @typedef {Object} TableConfig
 * @property {ApplicationConfig} app
 * @property {TestingConfig} testing
 * @example
 * {
        app: {
            rowHeight: 60,
            nrVisibleRows: 8,
            filterObj: {
                ColumnFilters: ['', '', '', '', '', ''],
                ColumnSorter:
                    {
                        column: undefined,
                        state:  ''
                    }
            },
            columnWidths: [150, 560, 250, 150, 560, 250, 560, 540]
        },
        testing: {
            SIMULATED_FETCH_DELAY: 150,
            TEST_FETCH_DELAY:      150 + 50
        }
    }
 */

/**
 * Describes the object used for initializing a table component.
 * @typedef {Object} ApplicationConfig
 * @property {number} rowHeight - Defines the height of the rendered table rows in pixels.
 * @property {number} nrVisibleRows - Defines the number of rendered rows the table displays at a time.
 * @property {Filter} filterObj
 * @property {ColumnWidths} columnWidths
 * @example
 * {
        rowHeight: 60,
        nrVisibleRows: 8,
        filterObj: {
            ColumnFilters: ['', '', '', '', '', ''],
            ColumnSorter:
                {
                    column: undefined,
                    state:  ''
                }
        },
        columnWidths: [150, 560, 250, 150, 560, 250, 560, 540]
    }
 */

/**
 * Describes the object used for testing a table component.
 * @typedef {Object} TestingConfig
 * @property {number} SIMULATED_FETCH_DELAY - Delay in ms that is used for testing simulating asynchronous data calls.
 * @example
 * {
        SIMULATED_FETCH_DELAY: 150
    }
 */

/**
 * Describes the object used for testing a table component.
 * @typedef {Object} TestingConfig
 * @property {number} TEST_FETCH_DELAY - Delay in ms that is used in tests while testing simulating asynchronous data calls.
 *                                       + 50ms to make sure the test runs AFTER data is fetched.
 * @example
 * {
        TEST_FETCH_DELAY: 150 + 50
    }
 */

/**
 * Describes a filter object.
 * @typedef {Object} Filter
 * @property {Array<string>} ColumnFilters
 * @property ColumnSorter ColumnSorter
 * @example
 * {
      ColumnFilters: [
        "", "u", "z", "", ""
      ],
      ColumnSorter:
        {
            column: 1,
            state: "desc"
        }
    }
 */

/**
 * Describes the object used for sorting.
 * @typedef {Object} ColumnSorter
 * @property {number} column - The index of the column the sorting applies to.
 * @property {('asc'|'desc'|'')} state - The sorting criteria.
 * @example
 * {
        column: 1,
        state: "desc"
    }
 */

/**
 * Describes the column width property.
 * @typedef ColumnWidths
 * @type {Array}
 * @example
 {
       [20, 40, 60, 80, 60, 40]
   }
 */

/**
 * Describes one entry of a dataset. All properties of a data entry must be accessible on the first level.
 * @typedef Entry
 * @type {Object}
 * @example
 * {id:0, name: "Hans Muster", email: "hans.muster@gmail.com", address: "9038 Metus Rd.", country: "New Zealand", region: "Melilla"}
 */

/**
 * @typedef { object } TableControllerType<T>
 * @template T
 * @property { ()  => T }      getFilteredData
 * @property { ()  => T }      updateData
 * @property { (T) => String } getEntryValueByIndexAndKey
 * @property { (T) => void }   setColumnFilter
 * @property { (T) => void }   setColumnSorter
 * @property { ()  => Number } getRenderedRowsCount
 * @property { (T) => void }   setNumberOfRenderedRows
 * @property { (T) => void }   setColumnWidths
 * @property { (T) => void }   scrollTopChangeHandler
 * @property { (T) => Number } handleIllegalItemIndex
 * @property { (T) => void }   updateItemIndex
 * @property { ()  => void }   resetPostFillSize
 * @property { (T) => void }   onDataReset
 * @property { ()  => void }   init
 *
 * @property { ()  => Object } getFilter
 * @property { ()  => Object } getEntryKeys
 * @property { ()  => Number } getKeysLength
 *
 * @property { (T) => void }   hasDataEntry
 * @property { ()  => Number } getTotalDataSize
 * @property { (T) => void }   setTotalDataSize
 * @property { ()  => Number } getCurrentDataSetSize
 * @property { (T) => void }   setCurrentDataSetSize
 *
 * @property { ()  => Number } getItemIndex
 * @property { ()  => Number } getScrollTop
 * @property { ()  => Number } getColumnWidths
 *
 * @property { ()  => Number } getPrefillHeight
 * @property { (T) => void }   setPrefillHeight
 * @property { ()  => Number } getPostfillInitialHeight
 * @property { (T) => void }   setPostfillInitialHeight
 * @property { ()  => Number } getPostfillHeight
 * @property { (T) => void }   setPostfillHeight
 * @property { ()  => Number } getNumberOfRenderedRows
 * @property { ()  => Number } getNumberOfVisibleRows
 * @property { (T) => void }   setNumberOfVisibleRows
 * @property { ()  => Number } getRowHeight
 * @property { ()  => Number } getViewPortHeight
 *
 * @property { (callback: onValueChangeCallback<Object>) => void } onFilterChanged
 * @property { (callback: onValueChangeCallback<Object>) => void } onEntryKeysChanged
 * @property { (callback: onValueChangeCallback<Object>) => void } onDataChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onItemIndexChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onNumberOfRenderedRowsChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onViewPortHeightChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onScrollTopChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onNumberOfVisibleRowsChanged
 * @property { (callback: onValueChangeCallback<Array<Number>>) => void } onColumnWidthsChanged
 */



/**
 * @typedef { object } TablePresentationModelType<T>
 * @template T
 * @property { (T) => void } initData
 * @property { (T) => void } getSingleDataEntry
 * @property { ()  => void } getData
 * @property { (T) => void } setDataEntry
 * @property { (T) => void } deleteDataEntry
 * @property { (T) => void } hasDataEntry
 * @property { (T) => void } onDataEntryClear
 * @property { ()  => Number } getDataSize
 * @property { (T) => void } onDataInit
 * @property { (T) => void } onDataChanged
 * @property { (T) => void } hasEntry
 *
 * @property { (T) => void }    setTotalDataSize
 * @property { ()  => Number }  getTotalDataSize
 * @property { (T) => void }    setCurrentDataSetSize
 * @property { ()  => Number }  getCurrentDataSetSize
 * @property { ()  => void }    getEntryKeys
 * @property { (T) => Object }  setEntryKeys
 * @property { (callback: onValueChangeCallback<Object>) => void } onEntryKeysChanged
 *
 * @property { ()  => Object }  getFilter
 * @property { (T) => void }    setFilter
 * @property { ()  => Number }  getItemIndex
 * @property { (T) => void }    setItemIndex
 *
 * @property { ()  => Number }  getScrollTop
 * @property { (T) => void }    setScrollTop
 * @property { ()  => Number }  getPrefillHeight
 * @property { (T) => void }    setPrefillHeight
 * @property { ()  => Number }  getPostfillInitialHeight
 * @property { (T) => void }    setPostfillInitialHeight
 * @property { ()  => Number }  getPostfillHeight
 * @property { (T) => void }    setPostfillHeight
 * @property { ()  => Number }  getViewPortHeight
 * @property { (T) => void }    setViewPortHeight
 * @property { ()  => Number }  getRowHeight
 * @property { (T) => void }    setRowHeight
 * @property { ()  => Number }  getNumberOfRenderedRows
 * @property { (T) => void }    setNumberOfRenderedRows
 * @property { ()  => Number }  getNumberOfVisibleRows
 * @property { (T) => void }    setNumberOfVisibleRows
 * @property { ()  => Array<Number> } getColumnWidths
 * @property { (T)  => void }   setColumnWidths
 *
 * @property { (callback: onValueChangeCallback<Object>) => void } onFilterChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onItemIndexChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onScrollTopChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onPrefillHeightChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onPostfillHeightChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onViewPortHeightChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onRowHeightChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onNumberOfRenderedRowsChanged
 * @property { (callback: onValueChangeCallback<Number>) => void } onNumberOfVisibleRowsChanged
 * @property { (callback: onValueChangeCallback<Array<Number>>) => void } onColumnWidthsChanged
 */