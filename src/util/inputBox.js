
export { inputBox }

/**
 * Component to test the mvc structure (active).
 */
const inputBox = mainController => {

    //View
    const inputBoxContainer = document.getElementById("input-box");

    const scrollToItemIndexButton    = document.createElement("button");
    const filterTestInputField       = document.createElement("input");
    const sortButton                 = document.createElement("button");
    const colWidthChanger            = document.createElement("input");

    scrollToItemIndexButton.textContent    = "Scroll to ItemIndex 42";
    filterTestInputField.placeholder       = "Filter by name";
    sortButton.textContent                 = "Sort column 3 asc";
    colWidthChanger.placeholder            = "Change ColWidth of Col1";

    inputBoxContainer.appendChild(scrollToItemIndexButton);
    inputBoxContainer.appendChild(filterTestInputField);
    inputBoxContainer.appendChild(sortButton);
    inputBoxContainer.appendChild(colWidthChanger);

    scrollToItemIndexButton.onclick = () => {
        mainController.updateItemIndex(42);
    }

    filterTestInputField.addEventListener("keyup", _ => {
        mainController.setColumnFilter(1, filterTestInputField.value);
    });

    sortButton.onclick = () => {
        mainController.setColumnSorter(3, "asc");
    }
    
    colWidthChanger.addEventListener("keyup", _ => {
        mainController.setColumnWidths(1, colWidthChanger.value);
    });
}
