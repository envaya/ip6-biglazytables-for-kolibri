import { TableController }        from "./controller/tableController.js";
import { inputBox }               from "../util/inputBox.js";
import { outputBox }              from "../util/outputBox.js";
import { TablePresentationModel } from "./model/tablePresentationModel.js";
import { TableHeadersProjector }  from "./projectors/header/tableHeadersProjector.js";
import { FiltersProjector }       from "./projectors/header/filtersProjector.js";
import { TableBodyProjector }     from "./projectors/body/tableBodyProjector.js";
import {TableFooterProjector}     from "./projectors/footer/tableFooterProjector.js";
import {testService}              from "./service/testService.js";
import {tableConfig}              from "./config.js";

/**
 * Define which service, model and controller is used.
 */
const service           = testService();
const presentationModel = TablePresentationModel(tableConfig);
const tableController   = TableController(presentationModel, service);

/**
 * Initialize controller and fetch initial data.
 */
tableController.init();

/**
 * Define HTML rootElement which represents the top-level HTML Element.
 * All other elements must be descendants of this element.
 */
const rootElement = document.getElementById('table');

/**
 * Initialize table Views.
 */
TableHeadersProjector(tableController, rootElement);
FiltersProjector     (tableController, rootElement);
TableBodyProjector   (tableController, rootElement);
TableFooterProjector (tableController, rootElement);

/**
 * Initialize separate views.
 */
inputBox (tableController);
outputBox(tableController);