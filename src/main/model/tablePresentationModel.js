/**
 * @module table/tablePresentationModel
 */

import {Observable} from "../../../kolibri/observable.js";
import { ObservableMap } from "../../util/util.mjs";

export { TablePresentationModel };

/**
 * Creates a presentation model for being used to bind against the table Projectors.
 * Access to this model is only provided by a {@link TableController}.
 * @param   {!TableConfig} tableConfig - Object to define parameters for the table.
 * @returns {TablePresentationModelType}
 */
const TablePresentationModel = tableConfig => {

    const data                  = ObservableMap(new Map());
    const totalDataSize         = Observable(0);
    const currentDataSetSize    = Observable(0);
    const entryKeys             = Observable([]);

    const filter                = Observable(tableConfig.app.filterObj);
    const itemIndex             = Observable(0);

    const scrollTop             = Observable(0);
    const prefillHeight         = Observable(0);
    let   postfillInitialHeight = 0;
    const postfillHeight        = Observable(0);
    const viewPortHeight        = Observable(0);
    const rowHeight             = Observable(tableConfig.app.rowHeight);
    const numberOfVisibleRow    = Observable(tableConfig.app.nrVisibleRows);
    const numberOfRenderedRows  = Observable(tableConfig.app.nrVisibleRows + 1);
    const columnWidths          = Observable(tableConfig.app.columnWidths);

    return {
        initData:                      data.init,
        getSingleDataEntry:            data.getItem,
        getData:                       data.get,
        setDataEntry:                  data.add,
        deleteDataEntry:               data.del,
        hasDataEntry:                  data.has,
        onDataEntryClear:              data.onClear,
        getDataSize:                   data.size,
        onDataInit:                    data.onInit,
        onDataChanged:                 data.onChange,
        hasEntry:                      data.has,

        setTotalDataSize:              totalDataSize        .setValue,
        getTotalDataSize:              totalDataSize        .getValue,
        setCurrentDataSetSize:         currentDataSetSize   .setValue,
        getCurrentDataSetSize:         currentDataSetSize   .getValue,
        getEntryKeys:                  entryKeys            .getValue,
        setEntryKeys:                  entryKeys            .setValue,
        onEntryKeysChanged:            entryKeys            .onChange,

        getFilter:                     filter               .getValue,
        setFilter:                     filter               .setValue,
        getItemIndex:                  itemIndex            .getValue,
        setItemIndex:                  itemIndex            .setValue,

        getScrollTop:                  scrollTop            .getValue,
        setScrollTop:                  scrollTop            .setValue,
        getPrefillHeight:              prefillHeight        .getValue,
        setPrefillHeight:              prefillHeight        .setValue,
        getPostfillInitialHeight:      ()     => postfillInitialHeight,
        setPostfillInitialHeight:      height => postfillInitialHeight = height,
        getPostfillHeight:             postfillHeight       .getValue,
        setPostfillHeight:             postfillHeight       .setValue,
        getViewPortHeight:             viewPortHeight       .getValue,
        setViewPortHeight:             viewPortHeight       .setValue,
        getRowHeight:                  rowHeight            .getValue,
        setRowHeight:                  rowHeight            .setValue,
        getNumberOfRenderedRows:       numberOfRenderedRows .getValue,
        setNumberOfRenderedRows:       numberOfRenderedRows .setValue,
        getNumberOfVisibleRows:        numberOfVisibleRow   .getValue,
        setNumberOfVisibleRows:        numberOfVisibleRow   .setValue,
        getColumnWidths:               columnWidths         .getValue,
        setColumnWidths:               columnWidths         .setValue,

        onFilterChanged:               filter               .onChange,
        onItemIndexChanged:            itemIndex            .onChange,
        onScrollTopChanged:            scrollTop            .onChange,
        onPrefillHeightChanged:        prefillHeight        .onChange,
        onPostfillHeightChanged:       postfillHeight       .onChange,
        onViewPortHeightChanged:       viewPortHeight       .onChange,
        onRowHeightChanged:            rowHeight            .onChange,
        onNumberOfRenderedRowsChanged: numberOfRenderedRows .onChange,
        onNumberOfVisibleRowsChanged:  numberOfVisibleRow   .onChange,
        onColumnWidthsChanged:         columnWidths         .onChange
    }
}