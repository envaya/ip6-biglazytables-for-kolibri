import { asyncRandomService } from "./asyncRandomService.js";
import { TableController }  from "../../src/main/controller/tableController.js";
import { inputBox }         from "../../src/util/inputBox.js";
import { outputBox }        from "../../src/util/outputBox.js";
import { TablePresentationModel } from "../../src/main/model/tablePresentationModel.js";
import {TableHeadersProjector} from "../../src/main/projectors/header/tableHeadersProjector.js";
import {FiltersProjector} from "../../src/main/projectors/header/filtersProjector.js";
import {TableBodyProjector} from "../../src/main/projectors/body/tableBodyProjector.js";
import {TableFooterProjector} from "../../src/main/projectors/footer/tableFooterProjector.js";
import {tableConfig} from "./asyncRandomConfig.js";

/**
 * Initialize service, model and controller.
 */
const services          = asyncRandomService(100);
const presentationModel = TablePresentationModel(tableConfig);
const tableController   = TableController(presentationModel, services);

tableController.init();

/**
 * Fetching the DOM element for hosting the table.
 */
const rootElement = document.getElementById('table');

/**
 * Initializing the views.
 */
TableHeadersProjector(tableController, rootElement);
FiltersProjector     (tableController, rootElement);
TableBodyProjector   (tableController, rootElement);
TableFooterProjector (tableController, rootElement);

/**
 * Initializing separate views for development and demonstration purposes.
 */
inputBox (tableController);
outputBox(tableController);